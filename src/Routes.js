import Home from "./components/Home";
import Posts from "./components/Posts";
import ViewPost from "./components/ViewPost";

export const Router = [
    {
        path: "/",
        name:"Home",
        component: <Home/>       
    },   
    {
        path: "/posts",
        name:"Posts",
        component: <Posts/>      
    },
    {
        path: "/posts-detail/:id",
        name:"Posts",
        component: <ViewPost/>      
    }
];