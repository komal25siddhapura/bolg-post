import { GET_ALL_POST_SUCCESS, START_LOADING, STOP_LOADING,PAGE_CHANGED } from "./post.types"

const initialState = {
    Post : [],
    IsLoading : false,
    Page : 1
}

const reducer = (state=initialState, action) => {

    switch(action.type)
    {
        case GET_ALL_POST_SUCCESS:
            return {...state, Post:action.payload};    
        case START_LOADING:
            return {...state, IsLoading:true};    
        case STOP_LOADING:
            return {...state, IsLoading:false};    
        case PAGE_CHANGED:
                return {...state, Page:action.payload};       
        default:
            return state;
    }
}

export default reducer;