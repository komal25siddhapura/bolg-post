import { combineReducers } from "redux";
import userReducer from '../user.redux/user.reducer';
import postReducer from '../post.redux/post.reducer';

const  rootReducer = combineReducers({
    user:userReducer,
    post:postReducer
});


export default rootReducer;