import { createStore, applyMiddleware } from "redux";
import rootReducer from "./rootReducer";
import loggingService from "../../services/loggingService";
import PostApiService from "../../services/PostApiService";

const store= createStore(
    rootReducer,
    applyMiddleware(
        loggingService,
        PostApiService
    ));

export default store;