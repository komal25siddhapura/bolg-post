import {Avatar ,Stack ,Badge, Chip, Grid} from '@mui/material';
import AddReactionIcon from '@mui/icons-material/AddReaction';
import { useNavigate } from 'react-router-dom';

export const CardDetailView = ({handleClickOpen, userDetail, postDetail}) =>{
    return(
            <div className='post-card' onClick={handleClickOpen}>
                <div className='post-userinfo'>                                                                       
                    <div>
                        <Avatar alt={userDetail?.username}  src={userDetail?.image} />
                    </div>
                    <div>
                        <p className='post-email'>{userDetail?.email}</p>
                        <p className='post-name'>{userDetail?.username}</p>
                    </div>
                </div>
                
                <p className='card-title'>{postDetail?.title}</p>
                <p className='card-content'>{postDetail?.body}</p>
                <div className='card-actions'>
                    <p></p>
                </div>
                <div className='card-actions'>                                        
                    <Stack direction="row" spacing={1}>
                        {
                            postDetail?.tags?.map((item)=><Chip key={item} label={item} color="primary" variant="outlined" />)
                        }                    
                    </Stack>                                
                    <Badge badgeContent={postDetail?.reactions} color="primary" overlap="circular"  anchorOrigin={{vertical: 'bottom',horizontal: 'right' }}>
                        <AddReactionIcon color="action"  fontSize='medium'/>
                    </Badge>                                                             
                </div>    
            </div>
    );
}


export const CustomCard= ({title, body, tags, reactions,id}) => {
    const nav=useNavigate();  
    return(
        <Grid container>
            <div className='post-card' onClick={() => nav(`/posts-detail/${id}`)}>                
                <p className='card-title'>{title}</p>                
                <p className='card-content'>{body}</p>               
                <div className='card-actions'>                   
                    <Stack direction="row" spacing={1}>
                        {
                            tags?.map((item)=><Chip key={item} label={item} color="primary" variant="outlined" />)
                        }                    
                    </Stack>                    
                    <Badge badgeContent={reactions} color="primary" overlap="circular"  anchorOrigin={{vertical: 'bottom',horizontal: 'right' }}>
                        <AddReactionIcon color="action"  fontSize='medium'/>
                    </Badge>                                         
                </div>    
            </div>
        </Grid>
    )
}