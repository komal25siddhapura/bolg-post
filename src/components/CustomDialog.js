import { Grid, DialogTitle, DialogContent, DialogActions, Dialog, Button} from '@mui/material';

export const ViewDialog =({open, handleClose, userDetail})=>{
    return(
        <Dialog open={open} onClose={handleClose} maxWidth='sm' fullWidth>    
            <DialogTitle>
                <div className='model-header'>
                    <img className='model-avatar' alt={userDetail?.username}  src={userDetail?.image} />
                    <p className='post-email'>{userDetail?.email}</p>
                    <p className='post-name'>{userDetail?.username}</p>
                </div>
                <hr/>
            </DialogTitle> 
            <DialogContent>  
                <Grid container>
                    <Grid item xs={6}>
                        <p><span className='dark-text'>First name : </span> <span className='dark-text1'>{userDetail?.firstName}</span></p>
                        <p><span className='dark-text'>Middle name : </span><span className='dark-text1'>{userDetail?.maidenName}</span></p>
                        <p><span className='dark-text'>Last name   : </span><span className='dark-text1'>{userDetail?.lastName}</span></p>
                    </Grid>
                    <Grid item xs={6}>
                        <p><span className='dark-text'>Phone : </span> <span className='dark-text1'>{userDetail?.phone}</span></p>
                        <p><span className='dark-text'>Gender : </span><span className='dark-text1'>{userDetail?.gender}</span></p>
                        <p><span className='dark-text'>Blood Group  : </span><span className='dark-text1'>{userDetail?.bloodGroup}</span></p>
                    </Grid>
                </Grid>                      
                
            </DialogContent>
            <DialogActions>
                <Button onClick={handleClose}>Close</Button>
            </DialogActions> 
        </Dialog>
    );
}