import { Link } from 'react-router-dom';

const Header =() => {
    return(        
        <div className='header-body'>            
            <div  className='menu-text'>
                <Link className='link-s' to="/">Home</Link>                       
                <Link className='link-s' to="/posts">Posts</Link>
            </div>    
        </div>  
    );
}

export default Header;