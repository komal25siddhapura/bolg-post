import { Routes, Route } from "react-router-dom";
    

const Navigation =({Router})=> {
    return(           
        <Routes>
            {
                Router.map((item)=>
                    <Route key={item?.name} exact path={item?.path} element={item.component}>                              
                    </Route>
                )
            }                 
        </Routes>
    );
}

export default Navigation;