import axios from "axios";

class API {
    constructor(url) {
        this.url = url;
      }

    async makeget() {
        return axios.get(this.url);
    }
}

export default API;